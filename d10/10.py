import math
import os


def read_file(filename):
    in_list = []
    with open(filename, "r") as f:
        data = f.readlines()
        for row in data:
            in_list.append(row.strip())
    return in_list


def eval_navigation_string(i_data):
    open_simbols = "[({<"
    close_simbols = "])}>"
    open_idxs = []
    for i in range(len(i_data)):
        if i_data[i] in "[({<":
            # open
            open_idxs.append(i)
        else:
            # close
            last_open_symbol = i_data[open_idxs[-1]]
            tmp_open_idx = open_simbols.find(last_open_symbol)
            tmp_closed_idx = close_simbols.find(i_data[i])
            if tmp_closed_idx != tmp_open_idx:
                # print(last_open_symbol, i_data[i])
                return i_data[i]
            else:
                open_idxs = open_idxs[:-1]
    return None


def eval_navigation_string_p2(i_data):
    open_simbols = "[({<"
    close_simbols = "])}>"
    open_idxs = []
    for i in range(len(i_data)):
        if i_data[i] in "[({<":
            # open
            open_idxs.append(i)
        else:
            # close
            last_open_symbol = i_data[open_idxs[-1]]
            tmp_open_idx = open_simbols.find(last_open_symbol)
            tmp_closed_idx = close_simbols.find(i_data[i])
            open_idxs = open_idxs[:-1]

    score = {"}": 3, ")": 1, ">": 4, "]": 2}
    score_val = 0
    res_simbols = ""
    for j in range(len(open_idxs)-1, -1, -1):
        op_j = i_data[open_idxs[j]]
        res_simbols = "{}{}".format(res_simbols, op_j)
        idx = open_simbols.find(op_j)
        score_val = score_val*5 + score[close_simbols[idx]]
    return score_val, res_simbols




data = read_file("input.txt")
# print(data)

# part-1
res = {"}": 0, ")": 0, ">": 0, "]": 0}
score = {"}": 1197, ")": 3, ">": 25137, "]": 57}
incomplete_lines = []
for i in range(len(data)):
    i_data = data[i]
    val = eval_navigation_string(i_data)
    if val is not None:
        res[val] += 1
    else:
        incomplete_lines.append(i_data)

syntax_error_score = 0
for key in res.keys():
    syntax_error_score += res[key]*score[key]
print("P1 - Syntax Error score is ", syntax_error_score)


# part - 2
res_scores = []
for i in range(len(incomplete_lines)):
    i_data = incomplete_lines[i]
    s, simbols = eval_navigation_string_p2(i_data)
    # print(s, simbols)
    res_scores.append(s)

res_scores.sort()
middle_idx = math.ceil(len(res_scores)/2)-1
print("P2 - Incomplete middle score is ", res_scores[middle_idx])
