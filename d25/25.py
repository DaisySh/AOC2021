import os
import numpy as np


def read_file(filename):
    in_list = []
    with open(filename, "r") as f:
        data = f.readlines()

        for row in data:
            row_val = []
            for i in row.strip():
                if i == ".":
                    row_val.append(0)
                elif i == "v":
                    row_val.append(1)
                else:
                    row_val.append(2)
            in_list.append(row_val)
    return np.array(in_list)


def move_downward(data_input):
    data = data_input.copy()
    x, y = np.where(data == 1)
    move_flag = False
    for i, j in zip(x, y):
        # new_coo = [i+1, j]
        if i+1 == data.shape[0]:
            # out of bounds, restart from the top
            if data_input[0, j] == 0:
                # do move
                data[i, j] = 0
                data[0, j] = 1
                move_flag = True
        else:
            # ok, check if nothing someone on the way
            if data_input[i+1, j] == 0:
                # do move
                data[i, j] = 0
                data[i+1, j] = 1
                move_flag = True
    return data, move_flag


def move_to_the_right(data_input):
    data = data_input.copy()
    x, y = np.where(data == 2)
    move_flag = False
    for i, j in zip(x, y):
        # new_coo = [i, j+1]
        if j+1 == data.shape[1]:
            # out of bounds, restart from the left
            if data_input[i, 0] == 0:
                # do move
                data[i, j] = 0
                data[i, 0] = 2
                move_flag = True
        else:
            # ok, check if nothing someone on the way
            if data_input[i, j+1] == 0:
                # do move
                data[i, j] = 0
                data[i, j+1] = 2
                move_flag = True
    return data, move_flag


def nice_print(data):
    for i in range(data.shape[0]):
        row = ""
        for j in range(data.shape[1]):
            val = data[i, j]
            if val == 1:
                row += "v"
            elif val == 2:
                row += ">"
            else:
                row += "."
        print(row)


def find_place_to_land(data):
    right_flag = True
    down_flag = True
    i = 0
    while right_flag or down_flag:
        i += 1
        data_right, right_flag = move_to_the_right(data)
        data, down_flag = move_downward(data_right)
        # print("After {} steps:".format(i))
        # nice_print(data)
        # print()
    return i


data = read_file("input.txt")
# data = read_file("input_small.txt")
# data = read_file("toy_example.txt")
i = find_place_to_land(data)
print("After step {} the submarine can land.".format(i))
