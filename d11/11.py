import numpy as np


def read_file(filename):
    in_list = []
    with open(filename, "r") as f:
        data = f.readlines()
        for i_row in data:
            local_row = []
            for i in i_row.strip():
                local_row.append(int(i))
            in_list.append(local_row)
    return in_list


def let_them_flash(sx, sy, data):
    for x, y in zip(sx, sy):
        x_list = [x, x, x-1, x-1, x-1, x+1, x+1, x+1]
        y_list = [y-1, y+1, y, y-1, y+1, y, y-1, y+1]
        for i, j in zip(x_list, y_list):
            if i >= 0 and j >= 0 and i < data.shape[0] and j < data.shape[1]:
                # valid coordinates
                if data[i, j] != 0:
                    data[i, j] += 1
    
    for x, y in zip(sx, sy):
        data[x, y] = 0
    

def count_flashes(data_np, steps):
    flash_count = 0
    for i_step in range(steps):
        # 1. copy struture
        local_data = data_np.copy()
        # 2. inc by 1 each element
        local_data += 1
        # 3. find idxs > 9
        id_x, id_y = np.where(local_data > 9)
        # 4. let them flash
        if len(id_x) > 0:
            flash_count += len(id_x)
            let_them_flash(id_x, id_y, local_data)
            # 5. check if idxs > 9 
            new_x, new_y = np.where(local_data > 9)
            while len(new_x) > 0:
                flash_count += len(new_x)
                let_them_flash(new_x, new_y, local_data)
                new_x, new_y = np.where(local_data > 9)
                
        data_np = local_data
    return flash_count


def steps_to_light(data_np):
    flag_stop = False
    steps = 0
    while not flag_stop:
        steps += 1
        # 1. copy struture
        local_data = data_np.copy()
        # 2. inc by 1 each element
        local_data += 1
        # 3. find idxs > 9
        id_x, id_y = np.where(local_data > 9)
        # 4. let them flash
        if len(id_x) > 0:
            let_them_flash(id_x, id_y, local_data)
            # 5. check if idxs > 9 
            new_x, new_y = np.where(local_data > 9)
            while len(new_x) > 0:
                let_them_flash(new_x, new_y, local_data)
                new_x, new_y = np.where(local_data > 9)
                
        data_np = local_data
        if data_np.sum() == 0:
            flag_stop = True
            return steps



# read file
data = read_file("input.txt")
data_np = np.array(data)

# part-1
steps = 100
flash_count = count_flashes(data_np, steps)
print("{} Flashes after {} steps.".format(flash_count, steps))

# part-2
s = steps_to_light(data_np)
print("All flashes at {} step.".format(s))
