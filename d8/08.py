import argparse
import os


def read_file(input_path):
    in_list = []
    with open(input_path, "r") as f:
        data = f.readlines()
        for row in data:
            tmp_list = row.split("|")
            keys = tmp_list[0].strip().split(" ")
            target = tmp_list[1].strip().split(" ")
            in_list.append((keys, target))
    return in_list


def get_idx(my_list, value):
    idx_list = []
    for i in range(len(my_list)):
        if my_list[i] == len(value):
            idx_list.append(i)
    return idx_list


def almost_zero_digit(values, known_keys):
    data_values = values.copy()
    # 4:9
    find_9 = 6
    key_9 = ""
    for item in data_values:
        v = set(item).difference(set(known_keys[4]))
        if len(v) < find_9:
            key_9 = item
            find_9 = len(v)
    known_keys[9] = key_9
    data_values.remove(key_9)
    # 0:8
    key_0 = ""
    for item in data_values:
        v = set(known_keys[8]).difference(set(item))
        v_intersection = v.intersection(set(known_keys[7]))
        if len(v_intersection) == 0:
            key_0 = item
    known_keys[0] = key_0
    data_values.remove(key_0)
    known_keys[6] = data_values[0]
    return known_keys


def almost_seven_digit(values, known_keys):
    data_values = values.copy()
    # 3:7
    find_3 = 5
    key_3 = ""
    for item in data_values:
        v = set(item).difference(set(known_keys[7]))
        if len(v) < find_3:
            key_3 = item
            find_3 = len(v)
    known_keys[3] = key_3
    data_values.remove(key_3)
    # 5:6
    key_5 = ""
    for item in data_values:
        v = set(known_keys[6]).difference(set(item))
        if len(v) == 1:
            key_5 = item
    known_keys[5] = key_5
    data_values.remove(key_5)
    known_keys[2] = data_values[0]
    return known_keys


def get_int_from_target(i_target, known_keys):
    target_val = ""
    for item in i_target:
        for i_key in known_keys:
            if len(item) == len(i_key):
                v = set(item).intersection(set(i_key))
                if len(v) == len(item):
                    idx = known_keys.index(i_key)
                    target_val += str(idx)
    return int(target_val)


def evaluate_digit(main_keys, i_key, i_target):
    tmp_keys = ["", "", "", "", "", "", "", "", "", ""]
    # list of segments by length
    digit_len = list(map(len, main_keys))
    # order input by length
    i_key.sort(key=len)
    last_keys = i_key.copy()
    five_keys = []
    six_keys = []
    for item in i_key:
        idx = get_idx(digit_len, item)
        if len(idx) == 1:
            # only one
            tmp_keys[idx[0]] = item
            last_keys.remove(item)
        if len(item) == 6:
            six_keys.append(item)
        if len(item) == 5:
            five_keys.append(item)
    tmp_keys = almost_zero_digit(six_keys, tmp_keys)
    tmp_keys = almost_seven_digit(five_keys, tmp_keys)
    return get_int_from_target(i_target, tmp_keys)


def dummy_indeces(main_keys, i_target):
    digit_len = [
        len(main_keys[1]),
        len(main_keys[4]),
        len(main_keys[7]),
        len(main_keys[8]),
    ]
    count_target = 0
    for i in i_target:
        if len(i) in digit_len:
            count_target += 1
    return count_target


def get_parser():
    parser = argparse.ArgumentParser(description="Display malfunction.")
    parser.add_argument("--input", type=str, help="input problem", required=True)
    parser.add_argument(
        "--problem", type=int, help="Which problem es: 1, 2", required=True
    )
    return parser


if __name__ == "__main__":
    parser = get_parser()
    args = parser.parse_args()
    keys = [
        "abcefg",
        "cf",
        "acdeg",
        "acdfg",
        "bcdf",
        "abdfg",
        "abdefg",
        "acf",
        "abcdefg",
        "abcdfg",
    ]
    if os.path.exists(args.input):
        digit_data = read_file(args.input)
        if args.problem == 1:
            res = 0
            for i_key, i_target in digit_data:
                res += dummy_indeces(keys, i_target)
            print("Count 1 - 4 - 7 - 8: {}".format(res))
        elif args.problem == 2:
            res = 0
            for i_key, i_target in digit_data:
                i_res = evaluate_digit(keys, i_key, i_target)
                res += i_res
            print("Sum targets: {}".format(res))
        else:
            parser.print_help()
    else:
        parser.print_help()
