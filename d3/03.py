import numpy as np
import sys

def read_file(file_path):
    in_list = []
    with open(file_path, 'r') as f:
        f1 = f.readlines()
        for item in f1:
            bin_val = item.strip()
            bin_string_list = []
            for c in bin_val:
                bin_string_list.append(int(c))
            in_list.append(bin_string_list)
    return in_list


# part -1
def count_bin(in_list):
    in_nparray = np.asarray(in_list)
    min_val = ""
    max_val = ""
    for i in range(in_nparray.shape[1]):
        tmp = in_nparray[:, i]
        one_count = tmp.sum()
        zero_count = len(tmp) - one_count
        if one_count > zero_count:
            max_val = "{}1".format(max_val)
            min_val = "{}0".format(min_val, zero_count)
        else:
            max_val = "{}0".format(max_val)
            min_val = "{}1".format(min_val)
    res = { "max_val": {"int": int(max_val, 2), "str":max_val},
            "min_val": {"int": int(min_val, 2), "str":min_val}}
    return res


# part -2
def count_bin_oxygen(in_list):
    in_nparray = np.asarray(in_list)
    idxs = list(range(in_nparray.shape[0]))
    tmp_in = np.array(in_nparray)
    for i in range(in_nparray.shape[1]):
        if tmp_in.shape[0] > 1:
            tmp = tmp_in[:, i]
            one_count = tmp.sum()
            zero_count = len(tmp) - one_count
            if zero_count > one_count:
                idxs = list(np.where(tmp==0)[0])
            else:
                idxs = list(np.where(tmp==1)[0])
            tmp_in = tmp_in[idxs, :]
        else:
            break
    res = list(tmp_in[0])
    oxy_val = ""
    for item in res:
        oxy_val = "{}{}".format(oxy_val, item)
    return int(oxy_val, 2)


def count_bin_c02(in_list):
    in_nparray = np.asarray(in_list)
    idxs = list(range(in_nparray.shape[0]))
    tmp_in = np.array(in_nparray)
    for i in range(in_nparray.shape[1]):
        if tmp_in.shape[0] > 1:
            tmp = tmp_in[:, i]
            one_count = tmp.sum()
            zero_count = len(tmp) - one_count
            if zero_count <= one_count:
                idxs = list(np.where(tmp==0)[0])
            else:
                idxs = list(np.where(tmp==1)[0])
            tmp_in = tmp_in[idxs, :]
        else:
            break
    res = list(tmp_in[0])
    co2_val = ""
    for item in res:
        co2_val = "{}{}".format(co2_val, item)
    return int(co2_val, 2)



in_list = read_file(sys.argv[1])
# part-1
res = count_bin(in_list)
print("max:{}, min:{}, x:{}".format(res["max_val"]["int"], res["min_val"]["int"], res["max_val"]["int"]*res["min_val"]["int"]))

# part-2
oxy_val = count_bin_oxygen(in_list)
c02_val = count_bin_c02(in_list)
print("oxy:{} c02:{} x:{}".format(oxy_val, c02_val, oxy_val*c02_val))
