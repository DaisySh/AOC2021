# AOC2021

Advent of code 2021 - https://adventofcode.com/2021

1. inc and sliding window
2. forward and depth
3. count bins c02 and oxy
4. squid wins
5. ocean floor grid
6. count fishes but ignore indexing
7. crabs alignment
8. digit similarities
9. high risk +
10. parse parenthesis
11. count flashes
12. I hate graphs and recursion
13. read the o
14.  
...
25. find a place to land
