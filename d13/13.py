import os
import numpy as np

def read_file(filename):
    dots_list = []
    folds = []
    max_x = 0
    max_y = 0
    with open(filename) as f:
        data = f.readlines()
        for row in data:
            tmp = row.strip()
            if len(tmp) > 0 and "fold" in tmp:
                # update folds
                tmp_fold = tmp.split(" ")
                coord_fold = tmp_fold[-1].split("=")
                folds.append((coord_fold[0], int(coord_fold[1])))
            elif len(tmp) > 0:
                # update coordinates
                coords = tmp.split(",")
                dots_list.append((int(coords[0]), int(coords[1])))
                if int(coords[0]) > max_x:
                    max_x = int(coords[0])
                if int(coords[1]) > max_y:
                    max_y = int(coords[1])
    return dots_list, folds, max_x, max_y


def fold_paper(max_folds, folds, dots_matrix):
    counter = 0
    for dir_fold, val in folds:
        counter += 1 
        # print(dir_fold, val)
        if dir_fold == "x":
            # fold left
            left_matrix = dots_matrix[:,0:val]
            right_matrix = dots_matrix[:,val+1:]
            dots_matrix = left_matrix + np.flip(right_matrix, axis=1)
            # print(dots_matrix)
            # print(left_matrix.shape, right_matrix.shape)
        else:
            # fold up
            up_matrix = dots_matrix[0:val,:]
            down_matrix = dots_matrix[val+1:,:]
            dots_matrix = up_matrix + np.flip(down_matrix, axis=0)
            # print(dots_matrix)
            # print(up_matrix.shape, down_matrix.shape)
        print(dots_matrix)
        if counter == max_folds:
            return np.count_nonzero(dots_matrix)
        
        
def fold_paper_v2(folds, dots_matrix):
    for dir_fold, val in folds:
        # print(dir_fold, val)
        if dir_fold == "x":
            # fold left
            left_matrix = dots_matrix[:,0:val]
            right_matrix = dots_matrix[:,val+1:]
            dots_matrix = left_matrix + np.flip(right_matrix, axis=1)
            # print(dots_matrix)
            # print(left_matrix.shape, right_matrix.shape)
        else:
            # fold up
            up_matrix = dots_matrix[0:val,:]
            down_matrix = dots_matrix[val+1:,:]
            dots_matrix = up_matrix + np.flip(down_matrix, axis=0)
            # print(dots_matrix)
            # print(up_matrix.shape, down_matrix.shape)
    return dots_matrix        


dots_list, folds, max_x, max_y = read_file("input.txt")

# part-1
dots_matrix = np.zeros((max_y+1, max_x+1))
for x, y in dots_list:
    dots_matrix[y, x] = 1
    
# count dots after 1 fold
# max_folds=1
# n_dots = fold_paper(max_folds, folds, dots_matrix)
# print("Number of dots after {} fold is: {}".format(max_folds, n_dots))


# part-2
dots_matrix = fold_paper_v2(folds, dots_matrix)
print(dots_matrix)
for i in range(dots_matrix.shape[0]):
    for j in range(dots_matrix.shape[1]):
        if dots_matrix[i, j] > 0 :
            print("o", end="")
        else:
            print(" ", end="")
    print()
