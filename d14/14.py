import numpy as np

def read_file(filename):
    in_pairs = {}
    template = []
    with open(filename) as f:
        data = f.readlines()
        
    for row in data:
        tmp = row.strip()
        if len(tmp) > 0:
            if "->" in tmp:
                pairs = tmp.split(" -> ")
                in_pairs[pairs[0]] = pairs[1]
            else:
                # add sequence
                for i in tmp:
                    template.append(i)
    return in_pairs, template


def first_step(in_pairs, template):
    eval_data = {k:0 for k in in_pairs.keys()}
    j = 0
    for i in range(1, len(template)):
        tmp = template[j]+template[i]
        # check if pair in in_pairs
        if tmp in eval_data.keys():
            eval_data[tmp] += 1
        j += 1
    return eval_data


def look_for_update(eval_data, in_pairs, steps):
    eval_copy = {iv:jv for iv, jv in eval_data.items()}
    for i in range(steps):
        eval_data = {iv:jv for iv, jv in eval_copy.items()}
        eval_copy = {iv:0 for iv, jv in eval_data.items()}
        for k, v in eval_data.items():
            if v > 0 :
                coo1 = k[0]+in_pairs[k]
                coo2 = in_pairs[k]+k[1]
                print(k, "->", coo1, coo2)
                if coo1 in eval_copy.keys():
                    eval_copy[coo1] += v
                if coo2 in eval_copy.keys():
                    eval_copy[coo2] += v
                # update eval copy
        s = sum(list(eval_copy.values()))
        print(i, s, eval_copy)
    return eval_copy


in_pairs, template = read_file("input_small.txt")
print(in_pairs, template)


res_target = {}
for k, v in in_pairs.items():
    res_target[v] = 0

print(res_target)
count_pairs = first_step(in_pairs, template)
print(count_pairs)

finds = look_for_update(count_pairs, in_pairs, 10)
print(finds)

for k, v in finds.items():
    idx_char = in_pairs[k]
    res_target[idx_char] += v
    
for k in template:
    res_target[k] += 1
    
print(res_target)
