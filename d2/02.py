def read_file(file_path):
    in_list = []
    with open(file_path, 'r') as f:
        f1 = f.readlines()
        for item in f1:
            instr = item.strip()
            tmp = instr.split(" ")
            in_list.append((tmp[0], int(tmp[1])))
    return in_list


# part-1
def simple_forward(instruction_list):
    h_val = 0
    d_val = 0
    for dir, val in instruction_list:
        if dir == "forward":
            h_val += val
        elif dir == "up":
            d_val -= val
        elif dir == "down":
            d_val += val
        else:
            print("Error on ", dir)
    return h_val, d_val

# part-2
def complicated_forward(instruction_list):
    h_val = 0
    d_val = 0
    aim_val = 0
    for dir, val in instruction_list:
        if dir == "forward":
            h_val += val
            d_val += aim_val * val
        elif dir == "up":
            aim_val -= val
        elif dir == "down":
            aim_val += val
        else:
            print("Error on ", dir)
    return h_val, d_val



instruction_list = read_file("input.txt")
# part-1
h_val, d_val = simple_forward(instruction_list)
print("h:{} d:{} x:{}".format(h_val, d_val, h_val*d_val))

# part-2
h_val, d_val = complicated_forward(instruction_list)
print("h:{} d:{} x:{}".format(h_val, d_val, h_val*d_val))
