import numpy as np
import argparse


def read_file(file_path):
    in_list = []
    with open(file_path, 'r') as f:
        f1 = f.readlines()
        for item in f1:
            counts = item.strip().split(",")
        for c in counts:
            in_list.append(int(c))
    return in_list

def update_count(fish_timer, idx_list):
    timer = [i-1 for i in fish_timer]
    for val in idx_list:
        timer[val] = 6
        timer.append(8)
    return timer

def zero_mask(fish_timer):
    idx_list = []
    count = 0
    for i in range(len(fish_timer)):
        if fish_timer[i] == 0:
            count += 1
            idx_list.append(i)
    return count, idx_list


def build_base(fish_timer):
    fish_age = np.zeros(9, dtype=int)
    for i in fish_timer:
        fish_age[i] +=1
    return fish_age



parser = argparse.ArgumentParser(description='Count fishes.')
parser.add_argument('--input', type=str,
                    help='input problem')
parser.add_argument('--days', type=int,
                    help='days to count')
parser.add_argument('--problem', type=int,
                    help='Which problem es: 1, 2')

args = parser.parse_args()
fish_timer = read_file(args.input)
days = args.days

if args.problem == 1:
    # part-1
    timer = fish_timer.copy()
    for i in range(days):
        count, idx_list = zero_mask(timer)
        if count > 0:
            timer = update_count(timer, idx_list)
        else:
            timer = [i-1 for i in timer]
    print("fishs after {} days -> {}".format(days, len(timer)))
elif args.problem == 2:
    # part-2
    fish_age = build_base(fish_timer)
    fish_age_update = fish_age.copy()
    for i_day in range(days):
        tmp = np.zeros(9, dtype=int)
        for i in range(len(tmp)-1, 0, -1):
            if fish_age_update[i] > 0:
                tmp[i-1] += fish_age_update[i]
        tmp[8] += fish_age_update[0]
        tmp[6] += fish_age_update[0]
        fish_age_update = tmp.copy()
    print("fishs after {} days -> {}".format(days,fish_age_update.sum()))
else:
    parser.print_help()