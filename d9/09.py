​import numpy as np


def read_input(filename):
    data = []
    with open(filename, "r") as f:
        lines = f.readlines()
        for i_line in lines:
            item = i_line.strip()
            tmp = []
            for i in item:
                tmp.append(int(i))
            data.append(tmp)
    return data


def get_basins(data, i, j, basin_elements):
    max_rows, max_cols = data.shape
    idx_i = [i-1, i, i , i+1]
    idx_j = [j, j-1, j+1, j]
    for k in range(len(idx_i)):
        if idx_i[k] >= 0 and idx_i[k] < max_rows and idx_j[k] >= 0 and idx_j[k] < max_cols:
            if data[idx_i[k], idx_j[k]] != 9 and (idx_i[k], idx_j[k]) not in basin_elements:
                basin_elements.append((idx_i[k], idx_j[k]))
                get_basins(data, idx_i[k], idx_j[k], basin_elements)
    return basin_elements


def get_coords_values(i, j, data):
    max_rows, max_cols = data.shape
    idx = []
    idx_i = [i-1, i, i , i+1]
    idx_j = [j, j-1, j+1, j]
    values = []
    for k in range(len(idx_i)):
        if idx_i[k] >= 0 and idx_i[k] < max_rows and idx_j[k] >= 0 and idx_j[k] < max_cols:
            idx.append((idx_i[k], idx_j[k]))
            values.append(data[idx_i[k], idx_j[k]])
    if data[i, j] < min(values):
        return True
    else:
        return False


def simple_high_risk(data):
    res = []
    res_idxs = []
    # study matrix
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            if get_coords_values(i, j, data):
                res.append(data[i, j])
                res_idxs.append((i, j))
    risk = sum(res) + len(res)
    return risk, res_idxs


def largest_basin(data):
    len_sets = []
    for item in res_idxs:
        tmp = get_basins(data, item[0], item[1], basin_elements=[item])
        len_sets.append(len(tmp))

    big_sets = sorted(len_sets)[-3:]
    return big_sets, np.prod(big_sets)


data = read_input("input.txt")
# convert data
data = np.array(data)

# problem - 1
res, res_idxs = simple_high_risk(data)
print("P1 high risk: {}.".format(res))

# problem - 2
l_sets, l_basin = largest_basin(data)
print("P2 largest basins: {}, x:{}.".format(l_sets, l_basin))
