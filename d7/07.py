import argparse
import numpy


def read_file(input_path):
    in_list = []
    with open(input_path, "r") as f:
        my_string = f.read()
        tmp_list = my_string.strip().split(",")
        for item in tmp_list:
            in_list.append(int(item))
    return in_list


def get_position(positions):
    min_pos = min(positions)
    max_pos = max(positions)
    position_cost = 0
    fuel_cost = numpy.infty
    for i in range(min_pos, max_pos):
        t = list(map(lambda c: abs(c - i), positions))
        if sum(t) < fuel_cost:
            fuel_cost = sum(t)
            position_cost = i
    return fuel_cost, position_cost


def eval_fuel_cost(positions, i):
    vals = []
    for item in positions:
        diff_val = abs(item - i)
        range_list = list(range(1, diff_val + 1))
        vals.append(sum(range_list))
    return vals


def get_position_v2(positions):
    min_pos = min(positions)
    max_pos = max(positions)
    position_cost = 0
    fuel_cost = numpy.infty
    for i in range(min_pos, max_pos):
        t = eval_fuel_cost(positions, i)
        if sum(t) < fuel_cost:
            fuel_cost = sum(t)
            position_cost = i
    return fuel_cost, position_cost


def get_parser():
    parser = argparse.ArgumentParser(description="Min Crab position.")
    parser.add_argument("--input", type=str, help="input problem", required=True)
    parser.add_argument(
        "--problem", type=int, help="Which problem es: 1, 2", required=True)
    return parser


if __name__ == "__main__":
    parser = get_parser()
    args = parser.parse_args()
    crab_pos = read_file(args.input)
    if args.problem == 1:
        fuel, pos = get_position(crab_pos)
        print("fuel:{} position to be: {}".format(fuel, pos))
    else:
        fuel, pos = get_position_v2(crab_pos)
        print("fuel:{} position to be: {}".format(fuel, pos))
