import numpy as np
def read_file(file_path):
    numbers2find = []
    in_list = []
    local_card = []
    with open(file_path, 'r') as f:
        f1 = f.readlines()
        for item in f1:
            item_val = item.strip()
            if item_val.find(",") > 0:
                # numbers to find
                    numbers2find = item_val.split(",")
            elif len(item_val) == 0:
                if len(local_card) != 0:
                    # update main list
                    in_list.append(local_card)
                # empty row
                local_card = []
            else:
                row = []
                for i in item_val.split(" "):
                    if i!="":
                        row.append(int(i))
                local_card.append(row)
        in_list.append(local_card)
    return numbers2find, in_list



def shout_bingo(mark_card, row_size, col_size):
    if np.max(mark_card["match_rows"]) == row_size or np.max(mark_card["match_cols"]) == col_size:
        return True
    return False


def eval_cards(cards_nparray, n_values):
    mark_cards = {}
    all_cards, rows, cols = cards_nparray.shape
    for i_card in range(all_cards):
        mark_cards[i_card] = {}
        mark_cards[i_card]["key"] = []
        mark_cards[i_card]["idxs"] = []
        mark_cards[i_card]["match_rows"] = np.zeros(rows)
        mark_cards[i_card]["match_cols"] = np.zeros(cols)

    bingo_card = {}
    for n in n_values:
        int_n = int(n)
        for i_card in range(all_cards):
            if not shout_bingo(mark_cards[i_card], rows, cols):
                local_card = cards_nparray[i_card, :, :]
                if int_n in local_card:
                    tmp = np.where(local_card == int_n)
                    tmp_arr = [tmp[0][0], tmp[1][0]]
                    mark_cards[i_card]["key"].append(int_n)
                    mark_cards[i_card]["idxs"].append(tmp_arr)
                    mark_cards[i_card]["match_rows"][tmp_arr[0]] += 1
                    mark_cards[i_card]["match_cols"][tmp_arr[1]] += 1
            else:
                bingo_card["last_idx"] = mark_cards[i_card]["key"][-1]
                bingo_card["card"] = i_card
                bingo_card["data"] = mark_cards[i_card]
                return bingo_card


def let_squid_win(cards_nparray, n_values):
    mark_cards = {}
    all_cards, rows, cols = cards_nparray.shape
    for i_card in range(all_cards):
        mark_cards[i_card] = {}
        mark_cards[i_card]["key"] = []
        mark_cards[i_card]["idxs"] = []
        mark_cards[i_card]["match_rows"] = np.zeros(rows)
        mark_cards[i_card]["match_cols"] = np.zeros(cols)

    cards_list = list(range(all_cards))
    bingo_card = {}
    for n in n_values:
        int_n = int(n)
        cards_list2 = cards_list[:]
        for i_card in cards_list2:
            if not shout_bingo(mark_cards[i_card], rows, cols):
                local_card = cards_nparray[i_card, :, :]
                if int_n in local_card:
                    tmp = np.where(local_card == int_n)
                    tmp_arr = [tmp[0][0], tmp[1][0]]
                    mark_cards[i_card]["key"].append(int_n)
                    mark_cards[i_card]["idxs"].append(tmp_arr)
                    mark_cards[i_card]["match_rows"][tmp_arr[0]] += 1
                    mark_cards[i_card]["match_cols"][tmp_arr[1]] += 1
            else:
                bingo_card["last_idx"] = mark_cards[i_card]["key"][-1]
                bingo_card["card"] = i_card
                bingo_card["data"] = mark_cards[i_card]
                cards_list.remove(i_card)
    return bingo_card

def get_output(bingo_card):
    tmp = cards_nparray[bingo_card["card"], :, :]
    all_sum_tmp = np.sum(tmp)
    all_sum_marked = np.sum(bingo_card["data"]["key"])
    res1 = all_sum_tmp - all_sum_marked
    res2 = res1 * bingo_card["last_idx"]
    return res1, res2



n_values, cards = read_file("input.txt")
cards_nparray = np.asarray(cards)
all_cards, rows, cols = cards_nparray.shape
# part-1
bingo_card = eval_cards(cards_nparray, n_values)
res1, res2 = get_output(bingo_card)
print(res1, res2)

# part-2
bingo_card_last = let_squid_win(cards_nparray, n_values)
res1, res2 = get_output(bingo_card_last)
print(res1, res2)
