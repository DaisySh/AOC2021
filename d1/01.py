def read_file(file_path):
    in_list = []
    with open(file_path, 'r') as f:
        f1 = f.readlines()
        for item in f1:
            in_list.append(int(item.strip()))
    return in_list

# part-1
def eval_increments(int_list):
    inc_val = 0
    prev_value = 0
    for i in int_list:
        if prev_value == 0:
            prev_value = i
        else:
            if i > prev_value:
                inc_val += 1
            prev_value = i
    return inc_val

# part-2
def sliding_sum_window(int_list, slide):
    sliding_list = []
    scarto = len(int_list) % slide
    for i in range(len(int_list)- scarto):
        tmp = sum(int_list[i:i+slide])
        sliding_list.append(tmp)
    return sliding_list


input = "input.txt"
int_list = read_file(input)
# part-1
inc_val = eval_increments(int_list)
print("part-1: ", inc_val)

# part-2
sliding_list = sliding_sum_window(int_list, slide=3)
inc_sliding_val = eval_increments(sliding_list)
print("part-2 ", inc_sliding_val)
