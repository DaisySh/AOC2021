import os
import networkx as nx
import matplotlib.pyplot as plt


def read_file(filename):
    in_list = []
    with open(filename, 'r') as f:
        data = f.readlines()
        for i_data in data:
            row = i_data.strip().split("-")
            in_list.append((row[0], row[1]))
    return in_list


def build_graph(input_edges, show=False):
    G = nx.Graph()
    for i in input_edges:
        G.add_edge(i[0], i[1])
    
    if show:
        n = G.number_of_nodes()
        e = G.number_of_edges()
        print("Nodes:{} and Edges:{}".format(n, e))
        nx.draw(G, with_labels=True, font_weight='bold')
        plt.show()
    return G



def eval_path(my_node, data_G, val, prec):
    # print(my_node)
    if my_node == "end":
        return val+1
    vicini = list(data_G[my_node])
    # print(vicini)
    prec = prec + [my_node]
    for k in vicini:
        if k != "start" and (not k.islower() or k == "end"):
            val = eval_path(k, data_G, val, prec)
        elif k not in prec:
            val = eval_path(k, data_G, val, prec)
    return val


def eval_path_v2(my_node, data_G, val, prec):
    # print(my_node)
    if my_node == "end":
        return val+1
    vicini = list(data_G[my_node])
    # print(vicini)
    if my_node.islower():
        prec = prec + [my_node]
    # print(prec)
    for k in vicini:
        if k == "start":
            pass
        elif k.isupper() or k == "end":
            val = eval_path_v2(k, data_G, val, prec)
        elif k not in prec:
            val = eval_path_v2(k, data_G, val, prec)
        elif len(prec) == len(set(prec)):
            val = eval_path_v2(k, data_G, val, prec)
    return val



data = read_file("input.txt")
data_G = build_graph(data, False)

# part-1
all_paths = eval_path("start", data_G, val=0, prec=[])
print("Paths to end: {}".format(all_paths))

# part-2
all_paths = eval_path_v2("start", data_G, val=0, prec=[])
print("More paths to end: {}".format(all_paths))
