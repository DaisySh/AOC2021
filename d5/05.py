import numpy as np

def read_file(file_path):
    in_list = []
    max_col = 0
    max_row = 0
    with open(file_path, 'r') as f:
        f1 = f.readlines()
        for item in f1:
            instr = item.strip()
            row = instr.split(" -> ")
            p1_vals = row[0].split(",")
            p2_vals = row[1].split(",")

            p1 = [int(p1_vals[0]), int(p1_vals[1])]
            p2 = [int(p2_vals[0]), int(p2_vals[1])]
            # if straight lines
            if p1[0] == p2[0] or p1[1] == p2[1]:
                local_max_rows = max([p1[0], p2[0]])
                if max_row < local_max_rows:
                    max_row = local_max_rows
                local_max_cols = max([p1[1], p2[1]])
                if max_col < local_max_cols:
                    max_col = local_max_cols
                in_list.append((p1, p2))
    return in_list, max_row, max_col

def read_file_p2(file_path):
    in_list = []
    max_col = 0
    max_row = 0
    with open(file_path, 'r') as f:
        f1 = f.readlines()
        for item in f1:
            instr = item.strip()
            row = instr.split(" -> ")
            p1_vals = row[0].split(",")
            p2_vals = row[1].split(",")

            p1 = [int(p1_vals[0]), int(p1_vals[1])]
            p2 = [int(p2_vals[0]), int(p2_vals[1])]
            local_max_rows = max([p1[0], p2[0]])
            if max_row < local_max_rows:
                max_row = local_max_rows
            local_max_cols = max([p1[1], p2[1]])
            if max_col < local_max_cols:
                max_col = local_max_cols
            in_list.append((p1, p2))
    return in_list, max_row, max_col


def get_connected_points(line):
    p1 = line[0]
    p2 = line[1]
    points = []
    if p1[0] == p2[0]:
        # horizontal line
        if p1[1] < p2[1]:
            # inc +1
            for i in range(p1[1], p2[1]+1):
                points.append((p1[0], i))
        else:
            # inc -1
            for i in range(p1[1], p2[1]-1, -1):
                points.append((p1[0], i))
    else:
        # vertical line
        if p1[0] < p2[0]:
            # inc +1
            for i in range(p1[0], p2[0]+1):
                points.append((i, p1[1]))
        else:
            # inc -1
            for i in range(p1[0], p2[0]-1, -1):
                points.append((i, p1[1]))
    return points


def get_connected_points_p2(line):
    p1 = line[0]
    p2 = line[1]
    points = []
    if p1[0] == p2[0]:
        # horizontal line
        if p1[1] < p2[1]:
            # inc +1
            for i in range(p1[1], p2[1]+1):
                points.append((p1[0], i))
        else:
            # inc -1
            for i in range(p1[1], p2[1]-1, -1):
                points.append((p1[0], i))
    elif p1[1] == p2[1]:
        # vertical line
        if p1[0] < p2[0]:
            # inc +1
            for i in range(p1[0], p2[0]+1):
                points.append((i, p1[1]))
        else:
            # inc -1
            for i in range(p1[0], p2[0]-1, -1):
                points.append((i, p1[1]))
    else:
        # diagonal line
        if p1[0] < p2[0]:
            rows = list(range(p1[0], p2[0]+1))
        else:
            rows = list(range(p1[0], p2[0]-1, -1))
        if p1[1] < p2[1]:
            cols = list(range(p1[1], p2[1]+1))
        else:
            cols = list(range(p1[1], p2[1]-1, -1))

        for i in range(len(rows)):
            points.append((rows[i], cols[i]))
    return points


# part-1
points_list, max_rows, max_cols = read_file("input.txt")
ocean_grid = np.zeros((max_rows+1, max_cols+1))
for line in points_list:
    local_points = get_connected_points(line)
    for i_local in local_points:
        ocean_grid[i_local[0], i_local[1]] += 1
res = np.count_nonzero(ocean_grid >= 2)
print("Only horizontal and vertical lines: ", res)


# part-2
points_list, max_rows, max_cols = read_file_p2("input.txt")
ocean_grid = np.zeros((max_rows+1, max_cols+1))
for line in points_list:
    local_points = get_connected_points_p2(line)
    for i_local in local_points:
        ocean_grid[i_local[0], i_local[1]] += 1
res = np.count_nonzero(ocean_grid >= 2)
print("With diagonal lines: ", res)
